package scala
import scala.Lista

import scala.util.Try

object Main extends App {

  val productos = Map[String,String](("Soup","0.65p"),("Bread","0.80p"),("Milk","£1.30"),("Apples","£1.00"),("Peanuts","2.00€"),("Banana","1.20€"))

  println("\nSUPERMARKET BASKET\n")

  productos.foreach {x => println(x._1 + " ---- " + x._2)}

  var texto = Console.readLine("\nDebes introducir los productos que desees en la forma: PriceBasket producto1 producto2 ...\n\n")

  var prodInput = (texto.split(" ")).toList
  Lista.supermarket(prodInput)

}
