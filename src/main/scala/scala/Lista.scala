package scala

import scala.Main._


object Lista {


  val productos = Map[String,String](("Soup","0.65p"),("Bread","0.80p"),("Milk","£1.30"),("Apples","£1.00"),("Peanuts","2.00€"),("Banana","1.20€"))


  def supermarket(lista:List[String]):Double = {

    var prodInput2 = (lista).toBuffer

    prodInput2(0) match {
      case "PriceBasket" => prodInput2.remove(0)
      case _=> throw new Exception("\n\n ERROR: Debes comenzar con la palabra 'PriceBasket'\n\n")

    }

    println(" ")

    var cesta = 0.0
    var vnewDescuento = 0
    var vnewDescuentoS = 0

    for(prod <- prodInput2) {

      var proceder = productos.keysIterator.exists(_.contains(prod))

      if (proceder == true) {

        for ((k, v) <- productos if (k == prod)) {

          if (v.indexOf("£") != -1) {
            var vnew = v.substring(1,(v.length))
            var vnew2:Double= vnew.toDouble
            vnew2 = (vnew2)*(0.9)
            println("Price of " + k + " is " + f"$vnew2%1.2f" + " €")
            k match {
              case "Apples" => vnewDescuento += 1
              case _=>
            }
            cesta += vnew2
          } else if (v.indexOf("€") != -1) {
            var vnew = v.substring(0,(v.length-1))
            var vnew2:Double= vnew.toDouble
            println("Price of " + k + " is " + f"$vnew2%1.2f"+ " €")
            cesta += vnew2
          } else {
            var vnew = v.substring(0,(v.length-1))
            var vnew2:Double= vnew.toDouble
            println("Price of " + k + " is " + f"$vnew2%1.2f"+ " €")
            k match {
              case "Soup" => vnewDescuentoS += 1
                cesta += vnew2
              case "Bread" => cesta += vnew2
              case _=> cesta += vnew2
            }
          }

        }

      } else println("Product '" + prod + "' doesn't exists")

    }

    var off = vnewDescuento * 0.1
    val off2 = 0.5
    var total = cesta - off
    var totalFinal = 0.0

    print("\n-- Subtotal: " + f"$cesta%1.2f" + " € ")

    if(vnewDescuento != 0){
          println("\n-- Apples 10% off: -" + off + " € ")
          totalFinal = cesta - off
    } else if(vnewDescuentoS == 2){
          println("\n-- Special offer. Half price of bread loaf getting two tins of soup: -" + off2 + " € ")
          totalFinal = cesta - 0.5
    } else {print("(No offers available)")
          totalFinal = cesta
    }

    println("\n-- Total price: "  + f"$totalFinal%1.2f" + " € ")

    return totalFinal
  }

}
