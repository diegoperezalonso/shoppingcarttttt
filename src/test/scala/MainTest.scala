package scala

import org.scalatest._

import scala.Main._


class MainTest extends FunSuite with Matchers {

  test("Two productos POUNDS") {

    val pruebaProductos = List[String]("PriceBasket","Apples","Apples")
    val result = Lista.supermarket(pruebaProductos)

    assert(result == 1.60)

  }

  test ("Two productos PENNIES") {

    val pruebaProductos = List[String]("PriceBasket","Soup","Bread")
    val result = Lista.supermarket(pruebaProductos)

    assert(result == 1.4500000000000002)

  }

  test ("Two productos EURO") {

    val pruebaProductos = List[String]("PriceBasket","Peanuts","Banana")
    val result = Lista.supermarket(pruebaProductos)

    assert(result == 3.20)
  }

  test ("Apples Offer") {

    val pruebaProductos = List[String]("PriceBasket", "Apples", "Apples")
    val result = Lista.supermarket(pruebaProductos)

    assert(result == 1.60)

  }

  test ("Bread Offer") {

    val pruebaProductos = List[String]("PriceBasket", "Apples", "Apples", "Bread")
    val result = Lista.supermarket(pruebaProductos)

    assert(result == 2.4)

  }

  test ("Wrong constructed order") {

    val pruebaProductos = List[String]("price basket", "Apples", "Apples")
    an [Exception] should be thrownBy(Lista.supermarket(pruebaProductos))

  }
}